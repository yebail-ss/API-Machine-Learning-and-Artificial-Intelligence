# 个人商业画布
![输入图片说明](https://images.gitee.com/uploads/images/2021/0417/113551_edd83847_4876229.png "个人商业画布.png")
# 迭代二增量改进说明
- 增加一张价值主张画布
- 增加两张用户画像
- 修改需求列表内容
- 细致描述核心交互一与核心交互二的文字内容
- 增加核心交互一与核心交互二的代码图片与原型图片
- 修改人工智能概率性考量部分的表格描述
- 增加了微软寻找相似度API与百度智能云人脸比对API的精确度对比（论证人工智能概率性考量中人脸比对API准确度高的观点）
- 改进具体学习/实践心得总结及感谢的模块
- 增加了互评温馨提示的模块，方便同学核对
- 改进md文档的排版
- **[Gitee的diff连结](https://gitee.com/yebail/API-Machine-Learning-and-Artificial-Intelligence/compare/5339591b20cba1106aa41db3b0f6ba6df64842e1...master)**
- [若需查看迭代一仓库，请点击此链接跳转](https://gitee.com/yebail/API-Machine-Learning-and-Artificial-Intelligence/tree/5339591b20cba1106aa41db3b0f6ba6df64842e1/)

# 产品文档PRD

## 接送安全在掌控

幼儿园家长接送孩子时，以往采取开条或者刷卡的方式，前者接送流程多且复杂，后者家长易出现忘记带卡或临时更换接送人但无法将卡转交的情况，于此同时放学期间人流量多且拥挤，接送效率低且有易混进不明人士的风险。而智能接送系统可以通过前期存储人脸的人脸数据实行人脸置信度比对，实行流畅接送，若临时更换接送人的，家长可手动添加接送人，解决刷卡烦恼，且在防作弊活体检测基础上与人脸对比的应用上，对可疑人士与不明人士进行警告与人脸数据登记并告知管理人员进行安全排查。

### 问题表述与需求列表

![价值主张画布](https://images.gitee.com/uploads/images/2020/0722/111246_b0484fdd_4876229.png "屏幕截图.png")

API 驱动之智能产品（人脸数据库、活体检测、人脸对比）可以有效缓解放学期间人流拥挤的情况和有效解决家长开条与刷卡烦恼，提高接送效率，并在满足基本接送任务的基础上给予了更安全的接送监管，有利于保障校园安全，让家长放心，也让学校更值得信赖。

![利益相关者](https://images.gitee.com/uploads/images/2020/0722/111302_1ac9c513_4876229.png "屏幕截图.png")

#### 问题表述

- 目标用户群

  - 核心用户：幼儿园接送家长
  - 主要用户：有安全需求的、规模较大的幼儿园

- 使用场景

  - 放学期间，家长接送孩子，通过在校门口处安装智能接送系统（脸数据库、活体检测、人脸对比）识别人脸数据，决定是否放行，若匹配成功且成功完成接送，则发送短信通知家长孩子已经离校。

  - 非放学期间，在校门口处安装智能识别系统，通过摄像头捕捉人脸信息，和数据库人脸数据库比对为何人，保证校内人员安全

- 任务

  - 家长接送幼儿园的孩子
  - 学校保证校内不混进不明人士，保证校内安全

- 用户痛点

  - 放学期间人流量多，人工审核接送人与孩子关系，效率低
  - 开条接送，申请流程与人工审核流程多且复杂
  - 刷卡接送，接送人更改与忘记带卡的情况，而这个应用可以通过app进行对接送人员进行设置从而实现更便捷额效果。
  - 放学期间人流量多，不明人士易混入，学校孩子安全无法保障                       

- 客户益点

  - 增加孩子在学校的安全性
  - 家长可实时掌握孩子的动态
  - 缓解放学期间人流拥挤的情况
  - 简化家长接送儿童的复杂流程

- **用户画像**

  ![用户画像](https://images.gitee.com/uploads/images/2020/0722/111319_de61a3ef_4876229.png "屏幕截图.png")

![用户画像](https://images.gitee.com/uploads/images/2020/0722/111333_46956c51_4876229.png "屏幕截图.png")

#### 需求列表

| 优先级 | 需求/用户场景                                                | 智能加值 | API类型               | ESG考虑                                                      |
| ------ | ------------------------------------------------------------ | -------- | --------------------- | ------------------------------------------------------------ |
| A      | 校门口监控内嵌了人脸比对功能，可以对需进出校门口处的人员的人脸数据与人脸数据库已有数据库信息进行比对，筛选人员的身份 | 是       | 百度智能云人脸比对API | 只对比人脸数据，并非对比照片，可保证隐私                     |
| **B**  | 家长或学校内部人员通过app向数据库拍照上传人脸数据，为人脸检测做准备 | 是       | 微软人脸数据库API     | 只收集人脸数据，并非收集照片，并且后期可手动删除数据，可保护用户的隐私 |
| C      | 当人脸数据库接送到家长或学校内部人员上传的人脸照片，会对照片中的人脸数据进行检测，并存储人脸数据信息，为人脸对比做准备 | 是       | 微软人脸检测API       | 只收集人脸数据，并非收集照片，并且后期可手动删除数据，可保护用户的隐私 |
| D      | 防作弊活体检测可以避免校门口处可疑人士企图利用别人的照片来伪造身份，这是人脸对比存在的基础。 | 否       | 百度智能云活体检测API | 一定程度上可以筛选可疑人士的数据，有利于提高智能接送系统的可信度 |


### 解决方案原型表述：

- 前期，家长只需要通过app拍摄上传接送人人脸数据到人脸数据库，填写孩子的专有token号实行关系绑定。（大多数情况下，接送人为父母或者爷爷奶奶，因而限制了直系亲属三人，降低校内出现危险人士的可能性。）
- 但若在后期出现了家长出现了临时更换接送人的情况，则需要再通过app拍摄上传人脸数据，再次填写token号实行关系绑定。（此为不常见情况，这种情况下系统默认为非直系亲属，接送期间，识别此类人脸数据后会发通知到老师手机，让老师将孩子送到校门口，既降低了家长后期添加人脸可能带来的校内安全风险，也完成了接送的任务）
- 简易交互界面流程图
  - [简易交互界面流程图_proceeson链接](https://www.processon.com/view/link/5f073b71e0b34d2a1cb9f0d2)

  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/112842_6ac96aa6_4876229.png "屏幕截图.png")

- **核心交互一**：用户拍摄上传人脸数据到数据库，数据库利用微软人脸检测API对数据进行检测，存储人脸数据信息


- **核心交互二**：校门口处监控捕捉门口人员数据，并对人员数据进行活体检测，人脸比对。（与数据库已有的人脸数据进行比对）


- **数据流程图**

![数据流程图](https://images.gitee.com/uploads/images/2020/0722/111541_9cdae3d2_4876229.png "屏幕截图.png")

#### 界面流程及关键智能交互

- 简易交互界面流程图
  - [简易交互界面流程图_proceeson链接](https://www.processon.com/view/link/5f073b71e0b34d2a1cb9f0d2)
![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/113438_0109c615_4876229.png "屏幕截图.png")

- **核心交互一**：用户拍摄上传人脸数据到数据库，数据库利用微软人脸检测API对数据进行检测，存储人脸数据信息

- **核心交互二**：校门口处监控捕捉门口人员数据，并对人员数据进行活体检测，人脸比对。（与数据库已有的人脸数据进行比对）

- [请查看迭代二界面流程图](http://yebail.gitee.io/api-machine-learning-and-artificial-intelligence)

 ![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/111621_3db15958_4876229.png "屏幕截图.png")

- 校门口处监控检测到人脸信息，将随机捕捉到的人脸数据传输到后台人脸数据库进行比对，输出到数据库后台管理系统

  - [人脸数据对比后台界面](http://yebail.gitee.io/api-machine-learning-and-artificial-intelligence/#id=7x2b34&p=%E4%BA%BA%E8%84%B8%E6%AF%94%E5%AF%B9)

- 检测用户上传的人脸图像的人脸数据，存储为人脸比对的类比数据

  - [检测人脸数据后台界面]([http://yebail.gitee.io/api-machine-learning-and-artificial-intelligence/#id=jv9rij&p=%E5%AD%98%E5%82%A8%E4%BA%BA%E8%84%B8%E7%9A%84%E4%BF%A1%E6%81%AF&g=1](http://yebail.gitee.io/api-machine-learning-and-artificial-intelligence/#id=jv9rij&p=存储人脸的信息&g=1))



#### 数据流程及关键智能API使用

- 两种不同或接近的价值主张 + 数据流程图DFD

  - 主价值主张一：通过微软人脸数据库API与查找相似度API、人脸检测API、百度智能云人脸比对API与活体检测API解决家长接送期间可能遇到的问题。
    - 人工审核，耗时长，效率低
    - 放学期间人流拥挤，可疑人士混入
    - 家长忘记带卡，耗费多余时间解决额外困难
  - 副价值主张：通过微软人脸数据库API与查找相似度API、人脸检测API、百度智能云人脸比对API与活体检测API保障校内安全，提高学校的安全性，吸引生源。
    - 对不在数据库的人脸数据进行登记和处理
    - 发现不明人士的数据，自动发送警报到管理员处，人工排查，双向保险

  

  **核心交互一：当人脸数据库接送到家长或学校内部人员上传的人脸照片，会对照片中的人脸数据进行检测，并存储人脸数据信息，为人脸对比做准备**

  - 输入数据：监控将人脸图片输入到人脸数据库
  - 输出数据：调用微软人脸检测API,对人脸数据进行检测，并将数据输出
    - 图一为代码输出结果
    - 图二为系统后台输出的可视化结果

  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0718/205249_13942f7f_4876229.png "屏幕截图.png")
  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0718/222918_079c4918_4876229.png "屏幕截图.png")

  **核心交互二：校门口处监控捕捉到人脸照片，将随机捕捉到的人脸数据传输到后台人脸数据库进行比对，输出到数据库后台管理系统**

  - 输入数据：校门口监控处捕捉人脸照片，将人脸照片数据传输给后台人脸数据库进行比对
  - 输出数据：调用微软人脸对比API,将传输过来的人脸数据与后台人脸数库已有数据进行对比
    - 图一为代码输出结果
    - 图二为系统后台输出的可视化结果

![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/111714_d2bea419_4876229.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0718/205236_547e575d_4876229.png "屏幕截图.png")



- **数据价值**

  - 系统会收集可疑/不明人士的人脸数据，形成可疑登记/危险登记排行榜，此数据可用于公安所用。

- 代码测试链接或数据可用列表

  - [微软人脸数据库API](https://westus.dev.cognitive.microsoft.com/docs/services/563879b61984550e40cbbe8d/operations/563879b61984550f30395251)
    ![输入图片说明](https://images.gitee.com/uploads/images/2020/0718/205318_04d3d2db_4876229.png "屏幕截图.png")

  - 调用结果：存储人脸数据到数据库
  - [ 微软人脸数据库API代码调用成果/个人测试代码](https://gitee.com/yebail/API-Machine-Learning-and-Artificial-Intelligence/blob/master/%E5%BE%AE%E8%BD%AF%E4%B8%8E%E7%99%BE%E5%BA%A6%E6%99%BA%E8%83%BD%E4%BA%91%E4%BB%A3%E7%A0%81%E8%B0%83%E7%94%A8%E6%83%85%E5%86%B5.ipynb)
  - ![输入图片说明](https://images.gitee.com/uploads/images/2020/0720/114200_d32b6cac_4876229.png "屏幕截图.png")
  - [微软人脸检测API](https://westus.dev.cognitive.microsoft.com/docs/services/563879b61984550e40cbbe8d/operations/563879b61984550f30395236)
![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/111814_4e404d8f_4876229.png "屏幕截图.png")
  - 调用结果：对人脸照片进行检测，获取人脸数据
  - [ 微软人脸检测API代码调用成果/个人测试代码](https://gitee.com/yebail/API-Machine-Learning-and-Artificial-Intelligence/blob/master/%E5%BE%AE%E8%BD%AF%E4%B8%8E%E7%99%BE%E5%BA%A6%E6%99%BA%E8%83%BD%E4%BA%91%E4%BB%A3%E7%A0%81%E8%B0%83%E7%94%A8%E6%83%85%E5%86%B5.ipynb)
  - ![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/112440_777d5bef_4876229.png "屏幕截图.png")
  - [百度智能云人脸对比API](https://ai.baidu.com/ai-doc/FACE/ak3co865s)
  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/111847_f546eea3_4876229.png "屏幕截图.png")
  - 调用结果（返回摄像机捕捉图片与数据库图片的相似度）
  - [ 百度智能云人脸对比API代码调用成果/个人测试代码](https://gitee.com/yebail/API-Machine-Learning-and-Artificial-Intelligence/blob/master/%E5%BE%AE%E8%BD%AF%E4%B8%8E%E7%99%BE%E5%BA%A6%E6%99%BA%E8%83%BD%E4%BA%91%E4%BB%A3%E7%A0%81%E8%B0%83%E7%94%A8%E6%83%85%E5%86%B5.ipynb)
  - ![输入图片说明](https://images.gitee.com/uploads/images/2020/0718/205412_0b515b78_4876229.png "屏幕截图.png")
  - ![输入图片说明](https://images.gitee.com/uploads/images/2020/0718/205422_700a0720_4876229.png "屏幕截图.png")
  - 调用结果（返回是否为活体数据）
  - [ 百度智能云活体检测API代码调用成果/个人测试代码](https://gitee.com/yebail/API-Machine-Learning-and-Artificial-Intelligence/blob/master/%E5%BE%AE%E8%BD%AF%E4%B8%8E%E7%99%BE%E5%BA%A6%E6%99%BA%E8%83%BD%E4%BA%91%E4%BB%A3%E7%A0%81%E8%B0%83%E7%94%A8%E6%83%85%E5%86%B5.ipynb)
  - ![输入图片说明](https://images.gitee.com/uploads/images/2020/0718/205430_3928dcb0_4876229.png "屏幕截图.png")
    ![输入图片说明](https://images.gitee.com/uploads/images/2020/0718/205440_b1a9f62a_4876229.png "屏幕截图.png")





#### **人工智能概率性考量**

| 计算机视觉人脸比对技术优势/用户痛点                          | 计算机视觉人脸数据库技术优势/用户痛点                        |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| 1. 效率高：代码调用速度高，相对于人工审核更快，在人流量大的放学期间，可以有效提高接送效率 | 1. 容量大：微软人脸数据库仅免费调用的API,就可达到最多1,000,000张面孔 |
| 2. 技术成熟：计算机视觉技术已经有了广泛的运用，人工智能与深度学习的发展促进了技术的成熟，因而，技术出错的概论较低，风险较小 | 2. 保证隐私：数据库不存储图像，只是提取的面部特征，可以保证用户的隐私 |
| 3. 准确度高：分析图片中人脸的遮挡度、模糊度、光照强度、姿态角度、完整度、大小等特征，基于输出的符合质量标准的图片，返回准确的相似度评分 |                                                              |

所以，该此类API安装于监控处，连接到后台数据，既可以满足家长的接送需求，又可以极大限度地解决家长接送孩子可能遇到的困扰，将接送孩子的过程中可能遇到的问题逐一解决，也在保证校园安全方面独具匠心。



#### **微软寻找相似度API与百度智能云人脸比对API准确度对比与论证**

以下分别对微软寻找相似度API**与百度智能云人脸比对API进行准确度的测试，评测结果为百度智能云人脸比对API准确度更高，因而我们选择它作为我们”接送安全在掌控“项目核心加值API

**微软寻找相似度API**

- [微软寻找相似度API](https://gitee.com/yebail/API-Machine-Learning-and-Artificial-Intelligence/blob/master/%E5%BE%AE%E8%BD%AF%E4%B8%8E%E7%99%BE%E5%BA%A6%E6%99%BA%E8%83%BD%E4%BA%91%E4%BB%A3%E7%A0%81%E8%B0%83%E7%94%A8%E6%83%85%E5%86%B5.ipynb)

- 输入数据：此为微软人脸比对API的代码数据，在进行相似度的比对前，我在数据库上传了十张照片，这十张照片均为神探夏洛克本人的图片。

![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/111945_cb50c7bb_4876229.png "屏幕截图.png")

- 输出结果：寻找相似度API传输出结果如下，可以看到相似度为“100%，94%，94%，92%，92%，91%，91%，91%，90%，89%，当相似度大于等于90%时，意味着识别正确，则此api的准确率为90%

 ![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/112000_8b17e946_4876229.png "屏幕截图.png")


- 百度智能云人脸比对API

  - [百度智能云人脸比对API调用代码链接](https://gitee.com/yebail/API-Machine-Learning-and-Artificial-Intelligence/blob/master/%E5%BE%AE%E8%BD%AF%E4%B8%8E%E7%99%BE%E5%BA%A6%E6%99%BA%E8%83%BD%E4%BA%91%E4%BB%A3%E7%A0%81%E8%B0%83%E7%94%A8%E6%83%85%E5%86%B5.ipynb)

  - 输入数据：此图为百度智能云人脸比对API代码调用的代码，我们同个获取到人脸图片的数据进行人脸相似度的比对

 ![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/112011_4daa6878_4876229.png "屏幕截图.png")

  - 输出结果：百度人脸比对API输出的相似度分别为97%，93%，93%，94%，93%，93%，93%，92%，94%，94%，当相似度大于等于90%时，意味着识别正确，则此api的准确率为100%

   ![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/112019_e63307b1_4876229.png "屏幕截图.png")

  

###  学习/实践心得总结及感谢 

- 这门课有一个概念让我有很深的反思，“智能是为了解决已有的需求，而不是为智能而智能”，廖老师在课上多次强调这个观点，让我对API智能加值有了更进一步的理解。我认为，现阶段，API所能做得事情便是尽最大可能地适应于已有的需求，让已经发展得很成熟地API深入需求场景,为人类生活创造更多的便捷。
- 在这个学期的前半个学期，我们进行了比较多的代码的调用，感谢老师在将计算机视觉的时候提供的代码示例，对我这次帮助很大，也很感谢一些同学在我学得有些艰难，甚至想放弃的时候，给予我的鼓励，很感谢“大风吹”过程同学们的参考意见，对于确认我这个项目的是否合理有很大的验证效果。
- 此次api项目主要调用的api代码来自百度智能云与微软，感谢开放平台提供的调用机会，下面附上所调用的api的链接

  - [azure 人脸数据库与人脸对比API与人脸检测API](https://westus.dev.cognitive.microsoft.com/docs/services/563879b61984550e40cbbe8d/operations/5a157b68d2de3616c086f2cc)
  - [百度智能云人脸对比API](https://ai.baidu.com/tech/face/compare?track=cp:ainsem|pf:pc|pp:chanpin-renlianshibie|pu:renlianshibie-jishu-renlianduibi|ci:|kw:10002229)
  - [百度智能云在线活体检测](https://cloud.baidu.com/doc/FACE/s/Zk37c1urr)

- 感谢网友提供的api调用代码示例

  - [百度人脸比对api代码调用](https://gitee.com/f549263766/FaceCompare/blob/master/Face++DetectCompare.py)
  - [在线活体检测api代码调用](https://blog.csdn.net/zuoguaishouxiao/article/details/105020192?ops_request_misc=&request_id=&biz_id=102&utm_term=%E7%99%BE%E5%BA%A6%E6%B4%BB%E4%BD%93%E6%A3%80%E6%B5%8B&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-2-105020192)

# 互评温馨提示
## URL汇总
- 共十二个链接，请点击查看
- [项目前期大风吹proceeson链接](https://www.processon.com/view/link/5f073b71e0b34d2a1cb9f0d2)
- [价值主张画布](https://www.processon.com/view/link/5f10418107912906d9a7dc98)
- [迭代二交互流程图](http://yebail.gitee.io/api-machine-learning-and-artificial-intelligence/#id=k07c0o&p=%E7%95%8C%E9%9D%A2%E5%85%B3%E7%B3%BB%E5%9B%BE)
- [数据流程图](https://www.processon.com/view/link/5ecccd835653bb79c100aee4)
- [利益相关者](https://www.processon.com/view/link/5f1960b4e401fd181ad4fe68)
- [用户画像](https://www.processon.com/view/link/5f073b171e08530ca80c1aea)
- [项目代码链接](https://gitee.com/yebail/API-Machine-Learning-and-Artificial-Intelligence/blob/master/%E5%BE%AE%E8%BD%AF%E4%B8%8E%E7%99%BE%E5%BA%A6%E6%99%BA%E8%83%BD%E4%BA%91%E4%BB%A3%E7%A0%81%E8%B0%83%E7%94%A8%E6%83%85%E5%86%B5.ipynb)
- [azure 人脸数据库与人脸对比API与人脸检测API](https://westus.dev.cognitive.microsoft.com/docs/services/563879b61984550e40cbbe8d/operations/5a157b68d2de3616c086f2cc)
- [百度智能云人脸对比API](https://ai.baidu.com/tech/face/compare?track=cp:ainsem|pf:pc|pp:chanpin-renlianshibie|pu:renlianshibie-jishu-renlianduibi|ci:|kw:10002229)
- [百度智能云在线活体检测](https://cloud.baidu.com/doc/FACE/s/Zk37c1urr)
- [百度人脸比对api代码调用](https://gitee.com/f549263766/FaceCompare/blob/master/Face++DetectCompare.py)
- [在线活体检测api代码调用](https://blog.csdn.net/zuoguaishouxiao/article/details/105020192?ops_request_misc=&request_id=&biz_id=102&utm_term=%E7%99%BE%E5%BA%A6%E6%B4%BB%E4%BD%93%E6%A3%80%E6%B5%8B&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-2-105020192)
## 关于图表
- 除去三个必要流程图与数据流程图，还有6个原创图表（图片+表格）
- 请同学翻阅查看，谢谢！
